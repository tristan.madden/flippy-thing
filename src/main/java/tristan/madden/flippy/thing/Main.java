package tristan.madden.flippy.thing;

import processing.core.*;

public class Main extends PApplet {

    Tile[][] tiles;
    public static PGraphics pg;
    public static PImage tex;
    public static int ROWS = 0;
    public static int COLS = 0;
    public static int MY_SCALE = 0;
    PerlinEffect perlin = new PerlinEffect(this);
    RandomEffect random = new RandomEffect(this);

    public static void main(String[] args) {
        PApplet.main("tristan.madden.flippy.thing.Main");
    }

    public void settings() {
        size(1024, 1024, FX2D);
        // fullScreen(FX2D);
    }

    public void setup() {
        imageMode(CENTER);

        MY_SCALE = (width / 128);
        COLS = (int) Math.ceil(width / MY_SCALE);
        ROWS = (int) Math.ceil(height / MY_SCALE) + 1;
        tiles = new Tile[COLS][ROWS];

        //tex = loadImage("textures/tex6.jpg");
        // tex.resize(MY_SCALE, MY_SCALE);
        pg = createGraphics(MY_SCALE, MY_SCALE);
        pg.beginDraw();
        //pg.image(tex, 0, 0);
        pg.strokeWeight(2);
        pg.stroke(255);
        //pg.noStroke();
        pg.fill(0);
        //pg.noFill();
        pg.arc(MY_SCALE, MY_SCALE, MY_SCALE, MY_SCALE, 0, (float) ((TWO_PI + PI) * 0.5));
        pg.arc(0, 0, MY_SCALE, MY_SCALE, 0, HALF_PI);
        pg.endDraw();

        for (int x = COLS - 1; x >= 0; x--) {
            for (int y = ROWS - 1; y >= 0; y--) {
                tiles[x][y] = new Tile(this, x, y, pg);
            }
        }
    }

    public void draw() {

        perlin.apply(tiles, 0.5f);
        //random.apply(tiles, 1);

        if (frameCount <= 3600) {
            //Processing is *much* faster at generating .tif files than .png files.
            //saveFrame("upload/frame-####.tif");
            surface.setTitle(frameCount + "");
        } else {
            exit();
        }

    }

}
