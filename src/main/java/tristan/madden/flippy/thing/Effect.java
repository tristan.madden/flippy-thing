package tristan.madden.flippy.thing;

public interface Effect {

    public void apply(Tile[][] tiles, float howMuch);

}
