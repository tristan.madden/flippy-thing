package tristan.madden.flippy.thing;

import processing.core.*;
import static tristan.madden.flippy.thing.Main.MY_SCALE;

public class Tile {

    //PHI
    float PHI = 1.61803398875f;
    float TWO_PHI = 3.2360679775f;
    //PI
    float HALF_PI = 1.57079632679f;
    float PI = 3.14159265359f;
    float TWO_PI = 6.28318530718f;
    //Variables
    boolean flip;
    int x;
    int y;

    float r;//rotation
    int rx;//rate of rotation
    float s;//scale
    float sx;//rate of scale

    PApplet parent;
    PGraphics img;

    Tile(PApplet p, int x, int y, PGraphics img) {
        this.parent = p;
        this.x = x;
        this.y = y;
        this.img = img;
        this.r = 0; //rotation
        this.rx = 0; //rate of rotation
        this.s = 1; //scale
        this.sx = 0; //rate of scale
        this.flip = false;

    }

    void update(int frames) {
        if (flip) {
            rx++;
            sx += (TWO_PHI / frames);
            if (rx % frames == 0) {
                flip = false;
                sx = 0;
            }
            r = parent.map(rx, 0, frames, 0, HALF_PI); //90-degree rotation
            s = parent.map((float) Math.sin(sx), 0, TWO_PHI, 1, 1.5f); //grows 50% larger during rotation
        }

    }

    void show() {
        parent.pushMatrix();
        parent.translate((MY_SCALE >> 1) + x * MY_SCALE, (MY_SCALE >> 1) + y * MY_SCALE);
        parent.rotate(r);
        parent.scale(s);
        parent.image(img, 0, 0);
        parent.popMatrix();
    }

    void flip() {
        flip = true;
    }

}
